<?php
	class Tables {
		private $body=array();
		private $info=array();
		private $rowsCounter=0;
		private $description;
		private $repeatInfo=50; //co ile wierszy wrzucic naglowek
		
		public function __construct($opis,$naglowek=NULL) {
			$this->description=$opis;
			if($naglowek>2) $this->repeatInfo=$naglowek;
		}
		
		public function addRow($data) {
			$this->rowsCounter++;
			foreach($data as $k=>$v) {
				$this->info[$this->rowsCounter][]=$k;
				$this->body[$this->rowsCounter][]=$v;
			}
		}
		
		private function makeVariables($variable) {
			$this->infoRow=' <tr>'."\n";
			$this->infoRow.='  <th></th>'."\n";
			foreach($this->info{1} as $key=>$val) {
				$this->infoRow.='  <th>'.$val.'</th>'."\n";
			}
			$this->infoRow.=' </tr>'."\n";

			$variable=str_replace('%colsCounter%',(count($this->info{1})+1),$variable);
			$variable=str_replace('%tableDescription%',$this->description,$variable);
			$variable=str_replace('%infoRow%',$this->infoRow,$variable);
			return $variable;
		}
		
		public function showTable() {
			$return ='<table class="minimalistBlack">'."\n";
			$return.=' <tr><th colspan="%colsCounter%">%tableDescription%</th></tr>'."\n";
			foreach($this->body as $key=>$val) {
				if(($key-1)%$this->repeatInfo==0) $return.='%infoRow%';
				$return.=' <tr>'."\n";
				$return.='  <td>'.$key.'</td>'."\n";
				foreach($val as $k=>$v) {
					$return.='  <td>'.$v.'</td>'."\n";
				}
				$return.=' </tr>'."\n";
			}			
			$return.='</table>'."\n";
			
			return $this->makeVariables($return);
		}
	}

	$fruits = array (
		array("a" => "orange1", "b" => "banana1", "c" => "apple1"),
		array("a" => NULL, "b" => "banana2", "c" => "apple2"),
		array("a" => "orange3", "b" => "banana3", "c" => "apple3"),
		array("a" => "orange4", "b" => "banana4", "c" => NULL),
		array("a" => "orange5", "b" => "banana5", "c" => "apple5"),
		array("a" => "orange6", "b" => "banana6", "c" => "apple6"),
		array("a" => "orange7", "b" => "banana7", "c" => "apple7"),
		array("a" => "orange8", "b" => NULL, "c" => "apple8"),
		array("a" => "orange9", "b" => "banana9", "c" => "apple9")
	);

	$tab=new Tables('testowy opis',5);
	foreach($fruits as $k=>$v) {
		$tab->addRow($v);
	}
	echo $tab->showTable();

	echo '<style>
table.minimalistBlack { border:3px solid #000000; text-align:left; border-collapse:collapse; }
table.minimalistBlack td, table.minimalistBlack th { border:1px solid #000000; padding:4px; }
table.minimalistBlack th { background:#CFCFCF; border-bottom:3px solid #000000; font-weight:bold; color:#000000;  text-align:center; }
table.minimalistBlack tr:nth-child(even) { background:#efefef; }
</style>';
?>
